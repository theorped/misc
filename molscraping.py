## PYTHON SETUP

# Install Anaconda which includes Python and the Spyder IDE: https://www.anaconda.com/download/ (3.7 version)

# Find the location of the python.exe file and add this to your computer's PATH environment variable
# (mine is in C:\Users\UTXP010\AppData\Local\Continuum\anaconda3\)
# Test this works by running the following in the command prompt: python --version (should not give an error)

# Install the packages below by running the below in the command prompt
# python -m pip install selenium 
# python -m pip install bs4 

# Import the Selenium 2 namespace (aka "webdriver")
from selenium import webdriver
from bs4 import BeautifulSoup, UnicodeDammit
import mkepub
import os
from unidecode import unidecode

os.chdir("C:\\Users\\Theo\\Documents\\2019\\Mother of Learning\\")

# download Chrome Driver from here https://sites.google.com/a/chromium.org/chromedriver/downloads
# need to copy the Chrome Driver a folder and specify the location below:
driver = webdriver.Chrome(r"C:\\Users\\Theo\\Documents\\2019\\chromedriver.exe") # this launches the browser!

chapter = 1

###########################
# STEP 1: Scrape chapters #
###########################
while chapter < 95:
    
    # Navigate to the page you want to scrape
    driver.get('https://www.fictionpress.com/s/2961893/' + str(chapter) + '/Mother-of-Learning')
    
    # Get chapter name
    title = 'Chapter ' + str(chapter) + ' - ' +driver.title[driver.title.find(': ')+2:].replace(', a fantasy fiction | FictionPress', '')
    
    if title == '':
        break
    
    # Get the page source html
    t = driver.page_source
    
    # Parse the html using BeautifulSoup (makes it easier to search)
    soup = BeautifulSoup(t, "lxml")
    divs = soup.find_all('div', {'id':'storytext'})
    
    # Filter out the elements we don't want to include in the epub
    include = [str(x) for x in divs[0].contents if x != '\n' and x != ' ' and x.text.find('Share this:') == -1 and x.text.find('Next Chapter') == -1]
    
    # Format the text as needed
    page = unidecode(''.join(include))
    page_write = page

    # Save text to an HTML file
    f = open(title.replace(".", "_").replace(':', '') + ".html","w+")
    f.write(page_write)
    f.close()
    
    # Try to move to the next chapter
    chapter += 1


###########################
# STEP 2: Make epub file  #
###########################

book = mkepub.Book(title='Mother of Learning', author = 'nobody103')

files = filter(os.path.isfile, os.listdir())
files = [os.path.join(f) for f in files] # add path to each file
files.sort(key=lambda x: os.path.getmtime(x))
    
chapters = [x for x in files if x.find('.html') != -1]

for ch in chapters:
    f = open(ch, 'r')
    book.add_page(title=ch, content=f.read())

book.save('Mother of Learning.epub')

