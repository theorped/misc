## PYTHON SETUP

# Install Anaconda which includes Python and the Spyder IDE: https://www.anaconda.com/download/ (3.7 version)

# Find the location of the python.exe file and add this to your computer's PATH environment variable
# (mine is in C:\Users\UTXP010\AppData\Local\Continuum\anaconda3\)
# Test this works by running the following in the command prompt: python --version (should not give an error)

# Install the packages below by running the below in the command prompt
# python -m pip install selenium 
# python -m pip install bs4 

# Import the Selenium 2 namespace (aka "webdriver")
from selenium import webdriver
from bs4 import BeautifulSoup


# download Chrome Driver from here https://sites.google.com/a/chromium.org/chromedriver/downloads
# need to copy the Chrome Driver a folder and specify the location below:
driver = webdriver.Chrome(r"C:\\Users\\UTXP010\\Documents\\chromedriver.exe") # this launches the browser!

# Navigate to the page you want to scrape
driver.get('https://australia.metservice.com/')

# Get the page source html
t = driver.page_source

# Parse the html using BeautifulSoup (makes it easier to search)
soup = BeautifulSoup(t, "lxml")
    

## Find temperature by parsing text
text = driver.find_element_by_tag_name('body').text
text.find('Feels like')
text[243:246]

## OR find it by looking up HTML elements
divs = soup.find_all('div', {'class':'local__now__forecast__temps__temp local__now__forecast__temps__temp--max'})
print(divs)
divs[0].contents[0]
